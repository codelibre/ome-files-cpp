/*
 * #%L
 * OME-FILES C++ library for image IO.
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <ome/files/FormatHandler.h>

#include <fmt/format.h>

namespace ome
{
  namespace files
  {

    bool
    FormatHandler::checkSuffix(const std::filesystem::path& name,
                               const std::filesystem::path& suffix)
    {
      bool match = true;

      std::filesystem::path filename(name);
      std::filesystem::path ext("dummy");
      ext.replace_extension(suffix); // Adds leading dot if missing

      while(true)
        {
          std::filesystem::path filename_ext = filename.extension();
          std::filesystem::path current_ext = ext.extension();
          filename.replace_extension();
          ext.replace_extension();

          if (filename_ext.empty() && current_ext.empty())
            break; // End of matches
          else if (!filename_ext.empty() && !current_ext.empty() &&
                   filename_ext == current_ext) // Match OK
            continue;

          // Unbalanced or unequal extensions.
          match = false;
          break;
        }

      return match;
    }

    bool
    FormatHandler::checkSuffix(const std::filesystem::path&              name,
                               const std::vector<std::filesystem::path>& suffixes)
    {
      for (const auto& suffix : suffixes)
        {
          if (checkSuffix(name, suffix))
            return true;
        }

      return false;
    }

    bool
    FormatHandler::checkSuffix(const std::filesystem::path&              name,
                               const std::vector<std::filesystem::path>& suffixes,
                               const std::vector<std::filesystem::path>& compression_suffixes)
    {
      if (checkSuffix(name, suffixes))
        return true;

      for (const auto& suffix : suffixes)
        {
          for (const auto& compsuffix : compression_suffixes)
            {
              std::filesystem::path fullsuffix(suffix);
              fullsuffix += std::filesystem::path(".");
              fullsuffix += compsuffix;

              if (checkSuffix(name, fullsuffix))
                return true;
            }
        }
      return false;
    }

    void
    FormatHandler::assertId(const std::optional<std::filesystem::path>& id,
                            bool                                        notNull)
    {
      if (!id && notNull)
        {
          throw std::logic_error("Current file should not be null; call setId() first");
        }
      else if (id && !notNull)
        {
          std::string fs = fmt::format("Current file should be null, but is '{}'; call close() first",
                                       id->string());
          throw std::logic_error(fs);
        }
    }

  }
}
